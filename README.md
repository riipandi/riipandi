<!-- <img src="https://media.giphy.com/media/SWoSkN6DxTszqIKEqv/giphy.gif" alt="Aris Ripandi"> -->

## <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="4%" /> &nbsp;Hi, I'm Aris Ripandi (ris)

![Profile views](https://komarev.com/ghpvc/?username=riipandi&color=blueviolet&style=flat)
[![Linkedin Badge](https://img.shields.io/badge/-aris--ripandi-blue?style=flat&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/aris-ripandi/)](https://www.linkedin.com/in/aris-ripandi)
[![Twitter Badge](https://img.shields.io/badge/-@riipandi-1ca0f1?style=flat&labelColor=1ca0f1&logo=twitter&logoColor=white&link=https://twitter.com/riipandi)](https://twitter.com/riipandi)
[![Mastodon Badge](https://img.shields.io/badge/Mastodon-%40ris-blueviolet?style=flat)](https://fosstodon.org/@ris)
[![Website Badge](https://img.shields.io/badge/ripandis.com-4384ff?style=flat&logo=appveyor&logoColor=white&link=https://ripandis.com/)](https://ripandis.com/)
[![Website Badge](https://img.shields.io/badge/Medium-2e3030?style=flat&logo=medium&logoColor=white)](https://medium.com/@riipandi/)
[![Github Sponsor](https://img.shields.io/static/v1?color=26B643&label=Sponsor&message=%E2%9D%A4&logo=GitHub&style=flat)](https://github.com/sponsors/riipandi)

I am a software engineer, educator, and Open Source enthusiast. I spend my time building things.

Joined Github about **11** years ago. Since then I pushed **5644** commits,
opened **13** issues, submitted **203** pull requests, received **261**
stars across **123** personal projects and contributed to **39**
public repositories.

_What I am working on:_

-   I am currently a Senior Product Engineer at [Zero One Group](https://zero-one-group.com/technology/), a digital agency based in Indonesia.
-   Previously I work as a software engineer at [FlowyTeam](https://www.flowyteam.com/), B2B SaaS focused on OKRs and Projects management tools.
-   On the other side, I am also building [Otentik Authenticator](https://otentik.app/), an Open Source [Google Authenticator](https://support.google.com/accounts/answer/1066447?hl=en&co=GENIE.Platform%3DAndroid)-compatible app.
-   I maintain OSS projects: [Laravel OptiKey](https://github.com/riipandi/laravel-optikey), [PHP Metabase](https://github.com/riipandi/php-metabase), [Packagist Mirror Site](https://packagist.pages.dev/), and [more](https://github.com/riipandi?tab=repositories&q=&type=source).
-   You can see the timeline of my work at [Polywork](https://poly.work/aris), and my resume on [LinkedIn](https://www.linkedin.com/in/aris-ripandi/).
-   Oh, and one last thing: I have a [personal website](https://ripandis.com/) and a technical [blog](https://riipandi.hashnode.dev/).

_What I am interested in:_

-   My domains of expertise are: ~~_PHP_~~ | JavaScript | TypeScript full-stack development, and application design.
-   In my spare time, I also like to learn about: Rust & Go programming, cloud computing related, and mentoring people.

#### Latest Blog Posts:

<!-- BLOG-POST-LIST:START -->
- [I&#39;m building a macOS app in a week with Tauri and Supabase.](https://riipandi.hashnode.dev/building-macos-app-in-a-week-with-tauri-and-supabase)
- [Deploying Authorizer to Fly.io](https://riipandi.hashnode.dev/deploying-authorizer-to-fly-dot-io)
- [Simplify Your Web Development Stack [For Windows Users]](https://riipandi.hashnode.dev/simplify-your-web-development-stack-for-windows-users)
- [Faktor Penting Branding Bisnis di Era Industri Digital](https://medium.com/@riipandi/faktor-penting-branding-bisnis-di-era-industri-digital-e49bcface3da?source=rss-fb74f4afc719------2)
<!-- BLOG-POST-LIST:END -->

#### Contact detail:

PGP Public Key: [`2874 4492 87E9 BDF3`](https://keybase.io/riipandi/pgp_keys.asc). More information available at:

```sh
npx riipandi
```

> My work timezone is Asia/Jakarta <a href="https://time.is/UTC+7" target="_blank" rel="noopener noreferrer">(UTC+7)</a>

#### GitHub Stats:

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=riipandi&layout=compact&hide_border=true&card_width=280)](https://s.id/1vBYf)
[![GitHub Stats](https://github-readme-stats.vercel.app/api?username=riipandi&hide_border=true&hide_title=true&show_icons=false&locale=en&theme=vue&text_bold=false&card_width=390)](https://s.id/1vBYA)

<!-- ![PHP](https://img.shields.io/static/v1?style=flat-square&label=%E2%A0%80&color=555&labelColor=%234F5D95&message=PHP%EF%B8%B162.6%25)
![JavaScript](https://img.shields.io/static/v1?style=flat-square&label=%E2%A0%80&color=555&labelColor=%23f1e05a&message=JavaScript%EF%B8%B110.7%25)
![HTML](https://img.shields.io/static/v1?style=flat-square&label=%E2%A0%80&color=555&labelColor=%23e34c26&message=HTML%EF%B8%B16.7%25)
![CSS](https://img.shields.io/static/v1?style=flat-square&label=%E2%A0%80&color=555&labelColor=%23563d7c&message=CSS%EF%B8%B15.7%25)
![C#](https://img.shields.io/static/v1?style=flat-square&label=%E2%A0%80&color=555&labelColor=%23178600&message=C%23%EF%B8%B14%25)
![Go](https://img.shields.io/static/v1?style=flat-square&label=%E2%A0%80&color=555&labelColor=%2300ADD8&message=Go%EF%B8%B13.9%25)
![TSQL](https://img.shields.io/static/v1?style=flat-square&label=%E2%A0%80&color=555&labelColor=%23e38c00&message=TSQL%EF%B8%B11.6%25)
![Other](https://img.shields.io/static/v1?style=flat-square&label=%E2%A0%80&color=555&labelColor=%23ededed&message=Other%EF%B8%B14.3%25)
 -->

---

<sub>🤫 Psst! If you like my work you can support me via [GitHub sponsors](https://github.com/sponsors/riipandi).
<br/>🤖 This README was generated using [teoxoy/profile-readme-stats](https://github.com/marketplace/actions/profile-readme-stats).</sub>
